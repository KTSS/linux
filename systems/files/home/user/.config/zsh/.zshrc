#!/usr/bin/env zsh

# Enable colors and change prompt:
autoload -U colors && colors  # Load colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
setopt autocd    # Automatically cd into typed directory.
stty stop undef  # Disable ctrl-s to freeze terminal.
setopt interactive_comments # Allows comments even in interactive shells

# History control
HISTFILE=~/.cache/zsh/history
HISTSIZE=5000000
SAVEHIST=$HISTSIZE
setopt INC_APPEND_HISTORY
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_SAVE_NO_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_REDUCE_BLANKS

# Load aliases and shortcuts if existent.
[  -f  "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc"        ]  &&  source  "$XDG_CONFIG_HOME/shell/aliasrc"
[  -f  "${XDG_CONFIG_HOME:-$HOME/.config}/shell/bm-dirs"        ]  &&  source  "$XDG_CONFIG_HOME/shell/bm-dirs"
[  -f  "${XDG_CONFIG_HOME:-$HOME/.config}/shell/bm-file"        ]  &&  source  "$XDG_CONFIG_HOME/shell/bm-file"
[  -f  "${XDG_CONFIG_HOME:-$HOME/.config}/shell/shortcutrc"     ]  &&  source  "$XDG_CONFIG_HOME/shell/shortcutrc"
[  -f  "${XDG_CONFIG_HOME:-$HOME/.config}/shell/zshnameddirrc"  ]  &&  source  "$XDG_CONFIG_HOME/shell/zshnameddirrc"

# Basic auto/tab complete:
autoload -U compinit
compinit -d ~/.cache/zsh/zcompdump-$ZSH_VERSION
zstyle ':completion:*' menu select cache-path $XDG_CACHE_HOME/zsh/zcompcache
compinit
zmodload zsh/complist
_comp_options+=(globdots)  # Include hidden files.

#Make cd behave like pushd
setopt auto_pushd

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey  -M  menuselect  'h'   vi-backward-char
bindkey  -M  menuselect  'k'   vi-up-line-or-history
bindkey  -M  menuselect  'l'   vi-forward-char
bindkey  -M  menuselect  'j'   vi-down-line-or-history
bindkey  -v              '^?'  backward-delete-char

# Change cursor shape for different vi modes.
function zle-keymap-select () {
    case $KEYMAP in
        vicmd) echo -ne '\e[1 q';;      # block
        viins|main) echo -ne '\e[5 q';; # beam
    esac
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Use lf to switch directories and bind it to ctrl-o
lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp" >/dev/null
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}
bindkey -s '^o' 'lfcd\n'

bindkey -s '^a' 'bc -lq\n'                  #opens calculator with ctrl+a

bindkey -s '^f' 'cd "$(dirname "$(sk)")"\n' #changes location to directory, but searching directories with skim

bindkey    '^[[P' delete-char

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# Load alias reminder
source /usr/share/zsh/plugins/zsh-you-should-use/you-should-use.plugin.zsh

# Load syntax highlighting; should be last.
source /usr/share/zsh/plugins/syntax-highlighting/syntax-highlighting.plugin.zsh 2>/dev/null

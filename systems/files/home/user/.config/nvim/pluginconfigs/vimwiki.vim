let g:vimwiki_list       =  [{    'path':       "${XDG_DATA_HOME:-$HOME/.local/share/}vimwiki",
                                \ 'path_html':  "${XDG_DATA_HOME:-$HOME/.local/share/}vimwiki_html",
                                \ }]
let g:vimwiki_url_maxsave = 0
map <leader>v :VimwikiIndex<CR>

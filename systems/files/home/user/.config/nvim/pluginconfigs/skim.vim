" skim ... even though the commands are prefixed with  fzf.  they work with
" skim, this is to keep maximum compatibility with fzf.vim

  " Besides, skim.vim add the interactive version of ag and rg function, you
  " could add these lines to your vimrc and try out. Since I only have ripgrep:
  command! -bang -nargs=* Rg call fzf#vim#rg_interactive(<q-args>, fzf#vim#with_preview('right:50%:hidden', 'alt-h'))
  " Empty value to disable preview window altogether
  let g:fzf_preview_window  = ''
  " Always enable preview window on the right with 40% width
  let g:fzf_preview_window  = 'right:40%'
  " [Buffers] Jump to the existing window if possible
  let g:fzf_buffers_jump    = 1
  " [[B]Commits] Customize the options used by 'git log':
  let g:fzf_commits_log_options = '--graph --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr"'
  " [Tags] Command to generate tags file
  let g:fzf_tags_command    = 'ctags -R'
  " [Commands] --expect expression for directly executing the command
  let g:fzf_commands_expect = 'alt-enter,ctrl-x'
  " files
  command! -bang -nargs=? -complete=dir Files call fzf#vim#files(<q-args>, <bang>0)
  " Source files
  command! -bang ProjectFiles call fzf#vim#files('~/.local/src', <bang>0)
  " Preview window with, e.g. bat
  command! -bang -nargs=? -complete=dir Files
    \ call fzf#vim#files(<q-args>, {'options': ['--layout=reverse', '--info=inline', '--preview', '~/.vim/plugged/fzf.vim/bin/preview.sh {}']}, <bang>0)
  " ripgrep with preview window
  command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always --smart-case -- '.shellescape(<q-args>), 1,
  \   fzf#vim#with_preview(), <bang>0)
  " Advanced ripgrep integration
  function! RipgrepFzf(query, fullscreen)
      let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case -- %s || true'
      let initial_command = printf(command_fmt, shellescape(a:query))
      let reload_command  = printf(command_fmt, '{q}')
      let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
      call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
  endfunction

  command! -nargs=* -bang RG call RipgrepFzf(<q-args>, <bang>0)
  " Mapping selecting mappings
  nmap <leader><tab> <plug>(fzf-maps-n)
  xmap <leader><tab> <plug>(fzf-maps-x)
  omap <leader><tab> <plug>(fzf-maps-o)
  " Insert mode completion
  imap <c-x><c-k> <plug>(fzf-complete-word)
  imap <c-x><c-f> <plug>(fzf-complete-path)
  imap <c-x><c-l> <plug>(fzf-complete-line)
  " Path completion with custom source command
  inoremap <expr> <c-x><c-f> fzf#vim#complete#path('fd')
  inoremap <expr> <c-x><c-f> fzf#vim#complete#path('rg --files')
  " Word completion with custom spec with popup layout option
  inoremap <expr> <c-x><c-k> fzf#vim#complete#word({'window': { 'width': 0.2, 'height': 0.9, 'xoffset': 1 }})
  " Replace the default dictionary completion with fzf-based fuzzy completion
  inoremap <expr> <c-x><c-k> fzf#vim#complete('cat /usr/share/dict/words')
  " Global line completion (not just open buffers. ripgrep required.)
  inoremap <expr> <c-x><c-l> fzf#vim#complete(skim#wrap({
    \ 'prefix': '^.*$',
    \ 'source': 'rg -n ^ --color always',
    \ 'options': '--ansi --delimiter : --nth 3..',
    \ 'reducer': { lines -> join(split(lines[0], ':\zs')[2:], '') }}))
  " Custom statusline
  function! s:fzf_statusline()
      " Override statusline as you like
      highlight fzf1 ctermfg=161 ctermbg=251
      highlight fzf2 ctermfg=23  ctermbg=251
      highlight fzf3 ctermfg=237 ctermbg=251
      setlocal statusline=%#fzf1#\ >\ %#fzf2#fz%#fzf3#f
  endfunction
  autocmd! User FzfStatusLine call <SID>fzf_statusline()

  " Mapping ,ff to skim window
  nnoremap  <leader>ff  <ESC>:SK<CR>

  " Mapping ,fr to RipGrep
  nnoremap  <leader>fr  <ESC>:Rg<CR>

  " Mapping ,fl to Lines (greps lines in all curretly opened buffers)
  nnoremap  <leader>fl  <ESC>:Lines<CR>

  " Mapping ,fh to vim commands history
  nnoremap  <leader>fh  <ESC>:History:<CR>

  " Mapping ,fb to show currently opened buffers
  nnoremap  <leader>fb  <ESC>:Buffers<CR>

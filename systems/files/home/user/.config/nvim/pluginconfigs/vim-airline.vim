" vim-airline
  set laststatus=2

  let g:airline_extensions#tabline#enabled=1
  let g:airline_powerline_fonts=1

  " Starts Vim Fugitive
    nmap  <leader>gs   :G<CR>
    nmap  <leader>gj   :diffget //2<CR>
    nmap  <leader>gf   :diffget //3<CR>
    nmap  <leader>gc   :Gcheckout<CR>
    nmap  <leader>gp   :Git push<CR>
    nmap  <leader>gd   :Git diff :0<CR>

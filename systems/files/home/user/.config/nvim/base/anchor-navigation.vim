" Navigating with anchors
inoremap <leader><leader> <Esc>/<++><Enter>"_c4l
vnoremap <leader><leader> <Esc>/<++><Enter>"_c4l
map      <leader><leader> <Esc>/<++><Enter>"_c4l

" general insert commands
inoremap <leader>anc <++>

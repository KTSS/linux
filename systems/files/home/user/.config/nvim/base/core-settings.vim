" Setting leader key
let mapleader =","

" Basic setings
set nocompatible
filetype plugin indent on
nnoremap c "_c
packloadall
set background=dark
set clipboard+=unnamedplus
set encoding=utf-8
set go=a
set laststatus=0
set mouse=a
set nohlsearch
set noruler
set noshowcmd
set noshowmode
set nowrap
set number
set relativenumber
set title
set colorcolumn=80
syntax on
colorscheme koehler

" Compile document, be it groff/LaTeX/markdown/etc.
  	map <leader>c :w! \| !compiler "<c-r>%"<CR>

" Cursor line:
  set cursorline
  set cursorcolumn

" Enable autocompletion
  set wildmode=longest,list,full

" Fixes splitting.
  set splitbelow splitright

" Improving vim just a little bit
    " 1 Yank, behave like normal, your ***ch
    nnoremap Y y$

    " 2 Keeping cursor centered
    nnoremap n nzzzv
    nnoremap N Nzzzv
    nnoremap J mzJ`z

    " 3 Undo break points
    inoremap , ,<c-g>u
    inoremap . .<c-g>u
    inoremap ! !<c-g>u
    inoremap ? ?<c-g>u
    inoremap ( (<c-g>u
    inoremap ) )<c-g>u
    inoremap [ [<c-g>u
    inoremap ] ]<c-g>u
    inoremap { {<c-g>u
    inoremap } }<c-g>u

    " 4 Jumplist mutations
    nnoremap <expr> j (v:count > 5 ? "m'" . v:count : "") . 'j'
    nnoremap <expr> k (v:count > 5 ? "m'" . v:count : "") . 'k'

    " 5 - Moving text without cluttering muh registars
    vnoremap  J          :m       '>+1<CR>gv=gv
    vnoremap  K          :m       '<-2<CR>gv=gv
    inoremap  <C-j>      <esc>:m  .+1<CR>==
    inoremap  <C-k>      <esc>:m  .-2<CR>==
    nnoremap  <leader>j  :m       .+1<CR>==
    nnoremap  <leader>k  :m       .-2<CR>==

" Open corresponding .pdf/.html or preview
  map <leader>p :!opout <c-r>%<CR><CR>

 " Shortcutting split navigation, saving a keypress:
   map <C-h> <C-w>h
   map <C-j> <C-w>j
   map <C-k> <C-w>k
   map <C-l> <C-w>l

" Perform dot commands over visual blocks:
	vnoremap . :normal .<CR>
" By making neovim keep the undo tree, it is possible to open a document after
" you closed it and still  undo  your changes. If the program was compiled with
" support for it
  if has("persistend_undo")
    set undodir="${XDG_CACHE_HOME:-$HOME/.cache}/neovim/undodir"
    set undofile
  endif

" Replace all is aliased to S.
  nnoremap S :%s//g<Left><Left>

" Save file as sudo on files that require root permission
  cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Spell-check set to <leader>o, 'o' for 'orthography':
  map <leader>o :setlocal spell! spelllang=en_us<CR>

" Tab settings
  set expandtab      " Convert all tabs typed into spaces
  set shiftround     " Always indent/outdent to the nearest tabstop
  set shiftwidth=4   " Indent/outdent by four colums
  set tabstop=4      " An indentation level for every four columns

" Turns off highlighting on the bits of code that are changed, so the line that is changed is highlighted but the actual text that has changed stands out on the line and is readable.
  if &diff
      highlight! link DiffText MatchParen
  endif

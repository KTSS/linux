"*.wiki

" Loads the header
  " appends template to new vimwiki files
    autocmd BufNewFile *.wiki source $XDG_CONFIG_HOME/nvim/base/wiki-header.txt

  " adds file name to title
    autocmd BufNewFile *.wiki execute "1," . 2 . "g/#title:.*/s@@#title:  " .expand("%")
  " adds the date for the creation date as the date
    autocmd BufNewFile *.wiki execute "1," . 2 . "g/#date:.*/s@@#date:   " .strftime("%Y-%m-%d %R:%S")

" Normal vimwiki editing
   " Link
     autocmd  Filetype  wiki  inoremap  <leader>li        [[]]<Esc>hi
   " Checkbox
     autocmd  Filetype  wiki  inoremap  <leader>cb        -<Space>[<Space>]<Space>
  " Headers (For Archwiki, don't use header level 1)
    autocmd  Filetype  wiki  inoremap  <leader>h1        =<Space>=<CR><CR><++><Esc>2k0a<Space>
    autocmd  Filetype  wiki  inoremap  <leader>h2        ==<Space>==<CR><CR><++><Esc>2k0ea<Space>
    autocmd  Filetype  wiki  inoremap  <leader>h3        ===<Space>===<CR><CR><++><Esc>2k0ea<Space>
    autocmd  Filetype  wiki  inoremap  <leader>h4        ====<Space>====<CR><CR><++><Esc>2k0ea<Space>
    autocmd  Filetype  wiki  inoremap  <leader>h5        =====<Space>=====<CR><CR><++><Esc>2k0ea<Space>
    autocmd  Filetype  wiki  inoremap  <leader>h6        ======<Space>======<CR><CR><++><Esc>2k0ea<Space>
  " Anchor
    autocmd  FileType  wiki  inoremap  <leader><leader>  <Esc>/<++><Enter>"_c4l

" Archwiki Editing
  " AUR
    autocmd  Filetype  wiki  inoremap  <leader>au        {{AUR\|}}<Space><++><Esc>6hi
  " Code blocks
    " Inline
      autocmd  Filetype  wiki  inoremap  <leader>ic        {{ic\|1=}}<Space><++><Esc>T=i
    " With header
      autocmd  Filetype  wiki  inoremap  <leader>hc        {{hc\|\|1=<CR><++><CR>}}<CR><CR><++><Esc>4k0f12ha
    " Without header
      autocmd  Filetype  wiki  inoremap  <leader>bc        {{bc\|1=<CR><CR>}}<CR><CR><++><Esc>3k0i
  " Manpage
    autocmd  Filetype  wiki  inoremap  <leader>ma        {{man\|\|<++>}}<Space><++><Esc>11hi
  " Notes
    autocmd  Filetype  wiki  inoremap  <leader>ne        {{Note\|}}<CR><CR><++><Esc>2kf\|a
    autocmd  Filetype  wiki  inoremap  <leader>np        {{Nota\|}}<CR><CR><++><Esc>2kf\|a
  " Package
    autocmd  Filetype  wiki  inoremap  <leader>pk        {{Pkg\|}}<Space><++><Esc>6hi
  " Tips
    autocmd  Filetype  wiki  inoremap  <leader>te        {{Tip\|}}<CR><CR><++><Esc>2kf\|a
    autocmd  Filetype  wiki  inoremap  <leader>tp        {{Dica\|}}<CR><CR><++><Esc>2kf\|a
  " Warnings
    autocmd  Filetype  wiki  inoremap  <leader>we        {{Warning\|}}<CR><CR><++><Esc>2kf\|a
    autocmd  Filetype  wiki  inoremap  <leader>wp        {{Atenção\|}}<CR><CR><++><Esc>2kf\|a

  " Basic formatting
    autocmd  Filetype  wiki  inoremap  <leader>ib        ''''''''''<Space><++><Esc>10ha
    autocmd  Filetype  wiki  inoremap  <leader>bd        ''''''<Space><++><Esc>8ha
    autocmd  Filetype  wiki  inoremap  <leader>it        ''''<Space><++><Esc>7ha
  " HTML
    autocmd Filetype   wiki  inoremap  <leader>hq        <q></q><Space><++><Esc>8hi
    autocmd Filetype   wiki  inoremap  <leader>hu        <u></u><Space><++><Esc>8hi
    autocmd Filetype   wiki  inoremap  <leader>hst       <s></s><Space><++><Esc>8hi
    autocmd Filetype   wiki  inoremap  <leader>hsb       <sub></sub><Space><++><Esc>10hi
    autocmd Filetype   wiki  inoremap  <leader>hsp       <sup></sup><Space><++><Esc>10hi
  " Links
    " To another page
      autocmd  Filetype  wiki  inoremap  <leader>lp        [[]]<Space><++><Esc>6hi
    " To another page (Choosing what to display)
      autocmd  Filetype  wiki  inoremap  <leader>lt        [[\|<++>]]<Space><++><Esc>11hi
    " To section on another page
      autocmd  Filetype  wiki  inoremap  <leader>ls        [[#<++>]]<Space><++><Esc>10hi
    " To section on same page
      autocmd  Filetype  wiki  inoremap  <leader>se        [[#]]<Space><++><Esc>6hi

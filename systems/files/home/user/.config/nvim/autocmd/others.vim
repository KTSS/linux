" Disables automatic commenting on newline:
  autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Vertically center document when entering insert mode
  autocmd InsertEnter * norm zz

" Runs a script that cleans out tex build files whenever I close out of a .tex file.
  autocmd VimLeave *.tex !texclear %

" Ensure files are read as what I want:
" vimwiki is controlled in its own vim file
  autocmd BufRead,BufNewFile *.md,*.markdown,*.rmd,/tmp/calcurse*,~/.calcurse/notes/* set filetype=markdown
  autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
  autocmd BufRead,BufNewFile *.tex set filetype=tex
  autocmd BufRead,BufNewFile *.wiki set filetype=wiki

" Automatically deletes all trailing whitespace and newlines at end of file on save.
  autocmd BufWritePre * %s/\n\+\%$//e
  autocmd BufWritePre * %s/\s\+$//e
  autocmd BufWritePre *.[ch] %s/\%$/\r/e
  autocmd BufWritepre * %s/\n\+\%$//e

" When shortcut files are updated, renew shell configs with new material:
  autocmd BufWritePost bm-file,bm-dirs !shortcuts
" Run xrdb whenever Xdefaults or Xresources are updated.
  autocmd BufWritePost *Xresources,*Xdefaults,*xresources,*xdefaults !xrdb %
" Autocompile dwmblocks when file is changed
  autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }

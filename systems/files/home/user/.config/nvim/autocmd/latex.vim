".tex
  "autocmd  FileType  tex  vnoremap  <leader>         <ESC>`<i\{<ESC>`>2la}<ESC>?\\{<CR>a
  " Word count:
  autocmd  FileType  tex  map       <leader><leader>o :w !detex \| wc -w<CR>
  " Basic formatting
  autocmd  FileType  tex  inoremap  <leader>bf       \textbf{}<Space><++><Esc>T{i
  autocmd  FileType  tex  inoremap  <leader>em       \emph{}<Space><++><Esc>T{i
  autocmd  FileType  tex  inoremap  <leader>it       \textit{}<Space><++><Esc>T{i
  autocmd  FileType  tex  inoremap  <leader>tt       \texttt{}<Space><++><Esc>T{i
  " Code snippets
  autocmd  FileType  tex  inoremap  <leader>a        \href{}{<++>}<Space><++><Esc>2T{i
  autocmd  FileType  tex  inoremap  <leader>bt       {\blindtext}
  autocmd  FileType  tex  inoremap  <leader>can      \cand{}<Tab><++><Esc>T{i
  autocmd  FileType  tex  inoremap  <leader>ce       \begin{center}<CR><CR>\end{center}<Esc>ki<Space><Space>
  autocmd  FileType  tex  inoremap  <leader>chap     \chapter{}<CR><CR><++><Esc>2kf}i
  autocmd  FileType  tex  inoremap  <leader>col      \begin{columns}[T]<CR>\begin{column}{.5\textwidth}<CR><CR>\end{column}<CR>\begin{column}{.5\textwidth}<CR><++><CR>\end{column}<CR>\end{columns}<Esc>5kA
  autocmd  FileType  tex  inoremap  <leader>con      \const{}<Tab><++><Esc>T{i
  autocmd  FileType  tex  inoremap  <leader>cp       \parencite{}<++><Esc>T{i
  autocmd  FileType  tex  inoremap  <leader>ct       \textcite{}<++><Esc>T{i
  autocmd  FileType  tex  inoremap  <leader>da       \documentclass{article}<CR><CR>\begin{document}<CR><CR><CR><CR>\end{document}<Esc>2ki
  autocmd  FileType  tex  inoremap  <leader>exe      \begin{exe}<CR>\ex<Space><CR>\end{exe}<CR><CR><++><Esc>3kA
  autocmd  FileType  tex  inoremap  <leader>fi       \begin{fitch}<CR><CR>\end{fitch}<CR><CR><++><Esc>3kA
  autocmd  FileType  tex  inoremap  <leader>fr       \begin{frame}<CR>\frametitle{}<CR><CR><++><CR><CR>\end{frame}<CR><CR><++><Esc>6kf}i
  autocmd  FileType  tex  inoremap  <leader>glos     {\gll<Space><++><Space>\\<CR><++><Space>\\<CR>\trans{``<++>''}}<Esc>2k2bcw
  autocmd  FileType  tex  inoremap  <leader>img      \begin{figure}<CR>\centering<CR>\includegraphics[width=0.5\textwidth]{}<CR>\caption{<++>}<CR>\end{figure}<CR><CR><++><Esc>4k$i
  autocmd  FileType  tex  inoremap  <leader>lab      \label{}<Esc>i
  autocmd  FileType  tex  inoremap  <leader>nl       \newline
  autocmd  FileType  tex  inoremap  <leader>np       \newpage<CR><CR>i
  autocmd  FileType  tex  inoremap  <leader>nu       $\varnothing$
  autocmd  Filetype  tex  inoremap  <leader>pkg      \usepackage{}<Esc>i
  autocmd  FileType  tex  inoremap  <leader>ref      \ref{}<Space><++><Esc>T{i
  autocmd  FileType  tex  inoremap  <leader>rn       (\ref{})<++><Esc>F}i
  autocmd  FileType  tex  inoremap  <leader>sc       \textsc{}<Space><++><Esc>T{i
  autocmd  FileType  tex  inoremap  <leader>st       <Esc>F{i*<Esc>f}i
  autocmd  FileType  tex  inoremap  <leader>te       \\ \hline<CR>
  autocmd  FileType  tex  inoremap  <leader>v        \vio{}<Tab><++><Esc>T{i
  autocmd  FileType  tex  inoremap  <leader>x        \begin{xlist}<CR>\ex<Space><CR>\end{xlist}<Esc>kA<Space>
" Diacritics/accents
  autocmd  FileType  tex  inoremap  á                \'{a}
  autocmd  FileType  tex  inoremap  à                \`{a}
  autocmd  FileType  tex  inoremap  ã                \~{a}
  autocmd  FileType  tex  inoremap  â                \^{a}
  autocmd  FileType  tex  inoremap  ç                \c{c}
  autocmd  FileType  tex  inoremap  é                \'{e}
  autocmd  FileType  tex  inoremap  ê                \^{e}
  autocmd  FileType  tex  inoremap  í                \'{i}
  autocmd  FileType  tex  inoremap  ó                \'{o}
  autocmd  FileType  tex  inoremap  õ                \~{o}
  autocmd  FileType  tex  inoremap  ô                \^{o}
  autocmd  FileType  tex  inoremap  ú                \'{u}
  autocmd  FileType  tex  inoremap  Á                \'{A}
  autocmd  FileType  tex  inoremap  À                \`{A}
  autocmd  FileType  tex  inoremap  Ã                \~{A}
  autocmd  FileType  tex  inoremap  Â                \^{A}
  autocmd  FileType  tex  inoremap  Ç                \c{C}
  autocmd  FileType  tex  inoremap  É                \'{E}
  autocmd  FileType  tex  inoremap  Ê                \^{E}
  autocmd  FileType  tex  inoremap  Í                \'{I}
  autocmd  FileType  tex  inoremap  Ó                \'{O}
  autocmd  FileType  tex  inoremap  Õ                \~{O}
  autocmd  FileType  tex  inoremap  Ô                \^{O}
  autocmd  FileType  tex  inoremap  Ú                \'{U}
" Lists
  autocmd  FileType  tex  inoremap  <leader>li       <CR>\item<Space>
  autocmd  FileType  tex  inoremap  <leader>ol       \begin{enumerate}<CR><CR>\end{enumerate}<CR><CR><++><Esc>3kA\item<Space>
  autocmd  FileType  tex  inoremap  <leader>ul       \begin{itemize}<CR><CR>\end{itemize}<CR><CR><++><Esc>3kA\item<Space>
" Sections
  autocmd  FileType  tex  inoremap  <leader>sec      \section{}<CR><CR><++><Esc>2kf}i
  autocmd  FileType  tex  inoremap  <leader>ssec     \subsection{}<CR><CR><++><Esc>2kf}i
  autocmd  FileType  tex  inoremap  <leader>sssec    \subsubsection{}<CR><CR><++><Esc>2kf}i
" Single and Double quotes
  autocmd  Filetype  tex  inoremap  <leader>sq       `'<Space><++><Esc>5hi
  autocmd  Filetype  tex  inoremap  <leader>dq       ``''<Space><++><Esc>6hi
" Tables
  autocmd  FileType  tex  inoremap  <leader>lt       \begin{longtable}[h]{}<CR>\caption{<++>}<CR>\label{<++>}<Space>\\<Space>\hline<CR><Space><Space><Space><Space>\textbf{<++>}<Space>&<Space>\textbf{<++>}<Space>\\<Space>\hline<CR><++><Space>&<Space><++><Space>\\<Space>\hline<CR>\end{longtable}<Esc>5k$i
  autocmd  FileType  tex  inoremap  <leader>ot       \begin{tableau}<CR>\inp{<++>}<Tab>\const{<++>}<Tab><++><CR><++><CR>\end{tableau}<CR><CR><++><Esc>5kA{}<Esc>i
  autocmd  FileType  tex  inoremap  <leader>tab      \begin{tabular}<CR><++><CR>\end{tabular}<CR><CR><++><Esc>4kA{}<Esc>i
" The magical comma, comma
  autocmd  FileType  tex  inoremap <leader><leader>  <Esc>/<++><CR>"_c4l

".'(md|markdown|rmd)'
autocmd  BufReadPost  *.md,markdown,rmd  colorscheme  koehler
autocmd  FileType     markdown           inoremap     <leader>1         #<Space><CR><CR><++><Esc>2k<S-a>
autocmd  FileType     markdown           inoremap     <leader>2         ##<Space><CR><CR><++><Esc>2k<S-a>
autocmd  FileType     markdown           inoremap     <leader>3         ###<Space><CR><CR><++><Esc>2k<S-a>
autocmd  FileType     markdown           inoremap     <leader>4         ####<Space><CR><CR><++><Esc>2k<S-a>
autocmd  FileType     markdown           inoremap     <leader>5         #####<Space><CR><CR><++><Esc>2k<S-a>
autocmd  FileType     markdown           inoremap     <leader>a         [](<++>)<Space><++><Esc>F]i
autocmd  FileType     markdown           inoremap     <leader>cb        -<Space>[ ]<Space>
autocmd  FileType     markdown           inoremap     <leader>f         +@fig:
autocmd  FileType     markdown           inoremap     <leader>i         ![](<++>){#fig:<++>}<Space><CR><CR><++><Esc>kkF]i
autocmd  FileType     markdown           inoremap     <leader>ol        1.<Space><CR><++><Esc>1k<S-a>
autocmd  FileType     markdown           inoremap     <leader>ul        +<Space><CR><++><Esc>1k<S-a>
autocmd  FileType     markdown           noremap      <leader>r         i---<CR>title:<Space><++><CR>author:<Space>"Bruno Alexander Brito"<CR>geometry:<CR>-<Space>top=30mm<CR>-<Space>left=20mm<CR>-<Space>right=20mm<CR>-<Space>bottom=30mm<CR>header-includes:<Space>\|<CR><Tab>\usepackage{float}<CR>\let\origfigure\figure<CR>\let\endorigfigure\endfigure<CR>\renewenvironment{figure}[1][2]<Space>{<CR><Tab>\expandafter\origfigure\expandafter[H]<CR><BS>}<Space>{<CR><Tab>\endorigfigure<CR><BS>}<CR><BS>---<CR><CR>

autocmd  Filetype     markdown,rmd       inoremap     <leader>1         #<Space><Enter><++><Esc>kA
autocmd  Filetype     markdown,rmd       inoremap     <leader>2         ##<Space><Enter><++><Esc>kA
autocmd  Filetype     markdown,rmd       inoremap     <leader>3         ###<Space><Enter><++><Esc>kA
autocmd  Filetype     markdown,rmd       inoremap     <leader>a         [](<++>)<++><Esc>F[a
autocmd  Filetype     markdown,rmd       inoremap     <leader>b         ****<++><Esc>F*hi
autocmd  Filetype     markdown,rmd       inoremap     <leader>e         **<++><Esc>F*i
autocmd  Filetype     markdown,rmd       inoremap     <leader>h         ====<CR><CR><++><Esc>F=hi
" autocmd  Filetype     markdown,rmd       inoremap     <leader>i         ![](<++>)<++><Esc>F[a
autocmd  Filetype     markdown,rmd       inoremap     <leader>l         --------<Enter>
autocmd  Filetype     markdown,rmd       inoremap     <leader>n         ---<Enter><Enter>
autocmd  Filetype     markdown,rmd       inoremap     <leader>s         ~~~~<++><Esc>F~hi
autocmd  Filetype     markdown,rmd       map          <leader>w         yiWi[<esc>Ea](<esc>pa)
autocmd  Filetype     rmd                inoremap     <leader>c         ```<cr>```<cr><cr><esc>2kO
autocmd  Filetype     rmd                inoremap     <leader>p         ```{python}<CR>```<CR><CR><esc>2kO
autocmd  Filetype     rmd                inoremap     <leader>r         ```{r}<CR>```<CR><CR><esc>2kO

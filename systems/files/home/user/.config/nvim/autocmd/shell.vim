" Shell
map     <leader>b i#!/bin/sh<CR><CR>
autocmd  FileType  sh  inoremap  <leader>sb        #!/bin/bash<CR><CR>
autocmd  FileType  sh  inoremap  <leader>fu        ()<Space>{<CR><Tab><++><CR>}<CR><CR><++><Esc>?()<CR>
autocmd  FileType  sh  inoremap  <leader>fo        for<Space><Space>in<Space><++><Space><++><CR>do<CR><++><CR>done<ESC>3k0f<Space>a
autocmd  FileType  sh  inoremap  <leader>i         if<Space>[<Space>];<Space>then<CR><++><CR>fi<CR><CR><++><Esc>?];<CR>hi<Space>
autocmd  FileType  sh  inoremap  <leader>ei        elif<Space>[<Space>];<Space>then<CR><++><CR><Esc>?];<CR>hi<Space>
autocmd  FileType  sh  inoremap  <leader>sw        case<Space>""<Space>in<CR><++>)<Space><++><Space>;;<CR><++><CR>esac<CR><CR><++><Esc>?"<CR>i
autocmd  FileType  sh  inoremap  <leader>ca        )<Space><++><Space>;;<CR><++><Esc>?)<CR>i

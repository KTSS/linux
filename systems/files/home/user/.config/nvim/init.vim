" Source basic settings
source $XDG_CONFIG_HOME/nvim/base/abbreviations.vim
source $XDG_CONFIG_HOME/nvim/base/anchor-navigation.vim
source $XDG_CONFIG_HOME/nvim/base/core-settings.vim

" Source auto commands
source $XDG_CONFIG_HOME/nvim/autocmd/bib.vim
source $XDG_CONFIG_HOME/nvim/autocmd/html.vim
source $XDG_CONFIG_HOME/nvim/autocmd/latex.vim
source $XDG_CONFIG_HOME/nvim/autocmd/markdown.vim
source $XDG_CONFIG_HOME/nvim/autocmd/others.vim
source $XDG_CONFIG_HOME/nvim/autocmd/roff.vim
source $XDG_CONFIG_HOME/nvim/autocmd/shell.vim
source $XDG_CONFIG_HOME/nvim/autocmd/xml.vim
source $XDG_CONFIG_HOME/nvim/autocmd/wiki.vim

" Source plugin settings
source $XDG_CONFIG_HOME/nvim/pluginconfigs/goyo.vim
source $XDG_CONFIG_HOME/nvim/pluginconfigs/limelight.vim
source $XDG_CONFIG_HOME/nvim/pluginconfigs/skim.vim
source $XDG_CONFIG_HOME/nvim/pluginconfigs/vim-airline.vim
source $XDG_CONFIG_HOME/nvim/pluginconfigs/vim-fugitive.vim
source $XDG_CONFIG_HOME/nvim/pluginconfigs/vimwiki.vim

# README #

Git repository for configuration files

### What is this repository for? ###

* Quick git repository to upload my version of Luke Smith's "voidrice" and LARBS. With a twist: ansible

### Contribution guidelines ###

Clone and adapt to your setup, that is what I did :)

### Who do I talk to? ###

* Bruno Alexander Brito <bruno.a.brito@outlook.com>
